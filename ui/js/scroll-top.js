jQuery( 
	function() 
	{
		jQuery( '[class*="cm-event-scrolltop"]' ).on( 
			'click', 
			function( event ) 
			{
				if ( this.hash !== "" ) 
				{
					event.preventDefault();
					
					let hash = this.hash;
					let prefix = 'cm-event-scrolltop-';
					let classes = jQuery( this ).attr( "class" ).split( /\s+/ );
					let delay = 700;
					
					for( let id in classes )
					{
						let classname = classes[ id ]; 
	        			if ( classname.startsWith( prefix ) )
	        			{
	        				delay = parseInt( classname.substring( prefix.length ) );
	        			}
					}
					let offsetTop = jQuery( hash ).offset().top;
					jQuery( 'html, body' ).animate(
						{ scrollTop: offsetTop },
						delay,
						'linear',
						function() { window.location.hash = hash; }
					);
				}
			}	 
		);
	}	
);